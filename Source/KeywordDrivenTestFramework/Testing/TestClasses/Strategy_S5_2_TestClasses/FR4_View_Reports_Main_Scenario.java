/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Strategy_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Strategy_S5_2_PageObjects.Strategy_S5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR4 View Reports - Main Scenario",
        createNewBrowserInstance = false
)
public class FR4_View_Reports_Main_Scenario extends BaseClass
{

    String error = "";
    String parentWindow;

    public FR4_View_Reports_Main_Scenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!View_Reports())
        {
            return narrator.testFailed("Navigate Strategy Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully captured Strategic Objectives Targets");

    }

    public boolean View_Reports()
    {
        //close window
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.close_window_record()))
        {
            error = "Failed to wait for close window option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.close_window_record()))
        {
            error = "Failed to click close window option";
            return false;
        }
        pause(3000);
        //Reports button
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.reportsBtn()))
        {
            error = "Failed to wait for 'Reports' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.reportsBtn()))
        {
            error = "Failed to click on 'Reports' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Reports' button.");
        parentWindow = SeleniumDriverInstance.Driver.getWindowHandle();
        pause(3000);
        //View reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.viewReportsIcon()))
        {
            error = "Failed to wait for 'View reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.viewReportsIcon()))
        {
            error = "Failed to click on 'View reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.continueBtn()))
        {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.continueBtn()))
        {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();

        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.StrategyReportHeader()))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.StrategyReportHeader()))
            {
                error = "Failed to wait for Strategy Report header to be present.";
                return false;
            }
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");
        narrator.stepPassedWithScreenShot("Viewing Rreports.");

        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.iframeXpath()))
        {
            error = "Failed to wait for frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Strategy_S5_2_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        //View full reports icon
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.viewFullReportsIcon()))
        {
            error = "Failed to wait for 'View full reports' icon.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.viewFullReportsIcon()))
        {
            error = "Failed to click on 'View full reports' icon.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'View full reports' icon.");
        SeleniumDriverInstance.switchToTabOrWindow();
        //Continue button
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.continueBtn()))
        {
            error = "Failed to wait for 'Continue' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.continueBtn()))
        {
            error = "Failed to click on 'Continue' button.";
            return false;
        }
        pause(5000);
        SeleniumDriverInstance.switchToTabOrWindow();
        narrator.stepPassedWithScreenShot("Successfully click 'Continue' button.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.BusinessUnitHeader()))
        {
            if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.BusinessUnitHeader()))
            {
                error = "Failed to wait for Engagements header to be present.";
                return false;
            }
        }

        narrator.stepPassedWithScreenShot("Viewing Full Rreports.");
        SeleniumDriverInstance.switchToDefaultContent();
        SeleniumDriverInstance.Driver.close();
        SeleniumDriverInstance.Driver.switchTo().window(parentWindow);

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully switched back");

        return true;

    }

}
