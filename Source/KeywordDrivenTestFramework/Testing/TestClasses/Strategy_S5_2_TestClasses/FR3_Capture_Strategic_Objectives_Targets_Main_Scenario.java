/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Strategy_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Strategy_S5_2_PageObjects.Strategy_S5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR3 Capture Strategic Objectives Targets - Main Scenario",
        createNewBrowserInstance = false
)
public class FR3_Capture_Strategic_Objectives_Targets_Main_Scenario extends BaseClass
{

    String error = "";

    public FR3_Capture_Strategic_Objectives_Targets_Main_Scenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Strategic_Objectives_Targets())
        {
            return narrator.testFailed("Navigate Strategy Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully captured Strategic Objectives Targets");

    }

    public boolean Strategic_Objectives_Targets()
    {
        //Navigate to add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Strategic_Objectives_Targets_Add()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Strategic_Objectives_Targets_Add()))
        {
            error = "Failed to click on add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked add button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.process_flow_Targets()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(300);
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.process_flow_Targets()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Action description
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Action_description()))
        {
            error = "Failed to wait for Action description field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.Action_description(), getData("Action description")))
        {
            error = "Failed to enter Action description :" + getData("Action description");
            return false;
        }
        narrator.stepPassedWithScreenShot("Action description  :" + getData("Action description"));
        //Department responsible
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Department_responsible_drop_down()))
        {
            error = "Failed to wait for Department responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Department_responsible_drop_down()))
        {
            error = "Failed to click on Department responsible dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Visible_Global_Company_Drop_Down()))
        {
            error = "Failed to wait for Department responsible dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Visible_Global_Company_Drop_Down()))
        {
            error = "Failed to click Department responsible dropdown";
            return false;
        }

        //Responsible person
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Responsible_person_drop_down()))
        {
            error = "Failed to wait for Responsible person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Responsible_person_drop_down()))
        {
            error = "Failed to click on Responsible person dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Responsible_person_option_targets(getData("Responsible person"))))
        {
            error = "Failed to wait for Responsible person dropdown option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Responsible_person_option_targets(getData("Responsible person"))))
        {
            error = "Failed to click Responsible person dropdown option";
            return false;
        }

        //Action due date
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Action_due_date()))
        {
            error = "Failed to wait for Action due date field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.Action_due_date(), startDate))
        {
            error = "Failed to enter Action due date :" + getData("Objectives");
            return false;
        }
        narrator.stepPassedWithScreenShot("Action due date  :" + startDate);

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.SaveBtnTargets()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.SaveBtnTargets()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.saveWait2()))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Strategy_S5_2_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Strategy_S5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        //close window

        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.close_window()))
        {
            error = "Failed to wait for close window option";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.close_window()))
        {
            error = "Failed to click close window option";
            return false;
        }

    
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.business_unit_drop_down()))
        {
            error = "Failed to wait for strategic objectives tab";
            return false;
        }

        
        pause(4000);
        narrator.stepPassedWithScreenShot("");
        return true;
    }

}
