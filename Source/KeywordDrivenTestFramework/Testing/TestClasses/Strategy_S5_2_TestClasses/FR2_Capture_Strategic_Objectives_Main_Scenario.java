/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Strategy_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Strategy_S5_2_PageObjects.Strategy_S5_2_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR2 Capture Strategic Objectives - Main Scenario",
        createNewBrowserInstance = false
)
public class FR2_Capture_Strategic_Objectives_Main_Scenario extends BaseClass
{

    String error = "";

    public FR2_Capture_Strategic_Objectives_Main_Scenario()
    {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!Strategic_Objectives())
        {
            return narrator.testFailed("Navigate Strategy Failed due - " + error);
        }

        if (getData("Check Tabs").equalsIgnoreCase("True"))
        {
            if (!Link_Process_Tab())
            {
                return narrator.testFailed("Linked Process/Activity Failed due - " + error);
            }
            if (!Link_Performance_Tab())
            {
                return narrator.testFailed("Linked Performance Indicators Failed due - " + error);
            }
        }
        if (getData("Supporting Documents").equalsIgnoreCase("True"))
        {
            if (!Supporting_Documents())
            {
                return narrator.testFailed("Supporting Documents Failed due - " + error);
            }
        }
        return narrator.finalizeTest("Successfully captured Strategic Objectives");

    }

    public boolean Strategic_Objectives()
    {

        //Navigate to add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Strategic_Objectives_Add()))
        {
            error = "Failed to wait for add button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Strategic_Objectives_Add()))
        {
            error = "Failed to click on add button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked add button.");

        //Process flow
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.process_flow_objectives()))
        {
            error = "Failed to wait for 'Process flow' button.";
            return false;
        }
        pause(300);
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.process_flow_objectives()))
        {
            error = "Failed to click on 'Process flow' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        //Objectives
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Objectives()))
        {
            error = "Failed to wait for Objectives field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.Objectives(), getData("Objectives")))
        {
            error = "Failed to enter Objectives :" + getData("Objectives");
            return false;
        }
        narrator.stepPassedWithScreenShot("Objectives  :" + getData("Objectives"));

        //Objectives category
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Objectives_category_drop_down()))
        {
            error = "Failed to wait for Objectives category dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Objectives_category_drop_down()))
        {
            error = "Failed to click on Objectives category dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Objectives category"))))
        {
            error = "Failed to wait for Objectives category dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Objectives category"))))
        {
            error = "Failed to click Objectives category dropdown";
            return false;
        }

        //Objectives owner 
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Objectives_owner_drop_down()))
        {
            error = "Failed to wait for Objectives owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Objectives_owner_drop_down()))
        {
            error = "Failed to click on Objectives owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Objective owner"))))
        {
            error = "Failed to wait for Objectives owner dropdown";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Strategy_owner_option(getData("Objective owner"))))
        {
            error = "Failed to click Objectives owner dropdown";
            return false;
        }
        narrator.stepPassedWithScreenShot("Objectives owner :" + getData("Objectives owner"));

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.SaveBtnProcess()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.SaveBtnProcess()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.saveWait2(), 4))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Strategy_S5_2_PageObjects.validateSave()))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Strategy_S5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        return true;
    }

    public boolean Link_Performance_Tab()
    {
        // Linked Performance Indicators 
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.Link_Performance_Tab(), 4))
        {
            error = "Failed to wait for Linked Performance Indicators tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Link_Performance_Tab()))
        {
            error = "Failed to click on Linked Performance Indicators tab";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Link_Performance_Select_All()))
        {
            error = "Failed to wait for Link Process/Activity list";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Link_Performance_Select_All()))
        {
            error = "Failed to click on Link Process/Activity select all";
            return false;
        }

        //Save
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.SaveBtnProcess()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.SaveBtnProcess()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.saveWait2(), 4))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.validateSave(), 4))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Strategy_S5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");

        pause(60000);
        pause(60000);
        narrator.stepPassedWithScreenShot("Linked Performance Indicators saved");
        return true;
    }

    public boolean Link_Process_Tab()
    {
       
        //Link Process/Activity
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Link_Process_Tab()))
        {
            error = "Failed to wait for Link Process/Activity tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Link_Process_Tab()))
        {
            error = "Failed to click on Link Process/Activity tab";
            return false;
        }
        
         //Process flow     
//        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.process_flow_objectives()))
//        {
//            error = "Failed to wait for 'Process flow' button.";
//            return false;
//        }
//        pause(3000);
//        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.process_flow_objectives()))
//        {
//            error = "Failed to click on 'Process flow' button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.Link_Process_Select_All()))
        {
            error = "Failed to wait for Link Process/Activity list";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.Link_Process_Select_All()))
        {
            error = "Failed to click on Link Process/Activity select all";
            return false;
        }

        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.SaveBtnProcess()))
        {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.SaveBtnProcess()))
        {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.saveWait2(), 4))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.validateSave(), 300))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }

        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Strategy_S5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        narrator.stepPassedWithScreenShot("Link Process  saved");

        return true;
    }

    public boolean Supporting_Documents()
    {
        //Supporting Documents tab
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.supporting_tab_2()))
        {
            error = "Failed to wait for Supporting Documents tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.supporting_tab_2()))
        {
            error = "Failed to click Supporting Documents tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to supporting documents.");

        //Add support document - Hyperlink
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.linkbox_2()))
        {
            error = "Failed to wait for 'Link box' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.linkbox_2()))
        {
            error = "Failed to click on 'Link box' link.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Strategy_S5_2_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter Url Title :" + getData("Title");
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //switch to the iframe
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        if (!SeleniumDriverInstance.switchToFrameByXpath(Strategy_S5_2_PageObjects.iframeXpath()))
        {
            error = "Failed to switch to frame.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        //Save supporting documents button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.SaveSupportingDocuments_SaveBtn_Objectives()))
        {
            error = "Failed to wait for 'Save supporting documents' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Strategy_S5_2_PageObjects.SaveSupportingDocuments_SaveBtn_Objectives()))
        {
            error = "Failed to click on 'Save supporting documents' button.";
            return false;
        }

        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Strategy_S5_2_PageObjects.saveWait2(), 4))
        {
            error = "Webside too long to load wait reached the time out";
            return false;
        }
        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementByXpath(Strategy_S5_2_PageObjects.validateSave(), 3))
        {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Strategy_S5_2_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        pause(300);
        narrator.stepPassed("Document Link :" + getData("Document Link"));
        narrator.stepPassed("Title :" + getData("Title"));

        return true;
    }
}
